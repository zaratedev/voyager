/**
 * Base Url
 */

/**
 * @method getRequest()
 *   @param string | 'request' | optional
 *
 *   @return
 *     => all request in array
 *
 * Example:
 *
 * fn.getRequest('id')
 */

/**
 * @method getParams()
 *   @param string | host/path/protocol/url | optional
 *
 *   @return array
 *     => host
 *     => path
 *     => protocol
 *     => url
 *
 * Example:
 *
 * fn.getParams('url')
 */

export class FNBaseUrl {

  constructor() {
    this.path      = window.location.pathname;
    this.pathArray = window.location.href.split( '/' );
    this.protocol  = this.pathArray[0];
    this.host      = this.pathArray[2];
    this.url       = this.protocol + '//' + this.host;
  }

  getRequest(request) {
    const getRequest = window.location.search.substring(1).split('&');

    // --

    let requestArray = {};
        request = request || false;

    // --

    $.each(getRequest, function(i, item) {
      const itemArray = item.split('=');
      requestArray[itemArray[0]] = itemArray[1];
    });

    if(request) {
      return requestArray[request];
    }

    return requestArray;
  }

  getParams(param) {
    const self = this;
    self.constructor();

    // --

    param = param || false;

    // --

    if(param) {
      return self[param];
    }

    return {
      'host'     : self.host,
      'path'     : self.path,
      'protocol' : self.protocol,
      'url'      : self.url
    }
  }
};

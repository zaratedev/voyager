/**
 * Environment
 */

/**
 * @attribute env-height
 *   @param string  | footer            | required
 *                    mainContainer
 *                    mainMenu
 *                    site
 *                    siteWithoutFooter
 *                    siteWithFooter
 *
 *   @param string  | min/max            | required
 *   @param numeric | breakpoint         | optional
 *
 * Example:
 *
 * <div env-height="siteWithFooter:min:768"></div>
 */

/**
 * @method getHeight()
 *   @param string | footer,            | optional
 *                   mainContainer,
 *                   mainMenu,
 *                   site,
 *                   siteWithoutFooter,
 *                   siteWithFooter
 *
 *   @return array
 *     => footer
 *     => mainContainer
 *     => mainMenu
 *     => site
 *     => siteWithoutFooter
 *     => siteWithFooter
 *
 * Example:
 *
 * console.log(fn.getHeight('footer'));
 */

export class FNEnvironment {

  constructor() {
    this.$footer        = $('footer');
    this.$mainContainer = $('.main-container');
    this.$mainMenu      = $('.main-menu');
    this.$site          = $('html');
    this.$environment   = $('[env-height]');
  }

  getHeight(el) {
    const self = this;
    self.constructor();

    // --

    el = el || false;

    // set classes

    if(el == 'siteWithFooter') {
      return self.$site.outerHeight() - self.$footer.outerHeight();
    } else if(el == 'siteWithoutFooter') {
      return self.$site.outerHeight();
    } else if(el) {
      return self['$'+el].outerHeight();
    }

    return {
      'footer'            : self.$footer.outerHeight(),
      'mainContainer'     : self.$mainContainer.outerHeight(),
      'mainMenu'          : self.$mainMenu.outerHeight(),
      'site'              : self.$site.outerHeight(),
      'siteWithoutFooter' : self.$site.outerHeight(),
      'siteWithFooter'    : self.$site.outerHeight() - self.$footer.outerHeight(),
    };
  }

  _setEnvironment() {
    const self = this;
    self.constructor();

    // --

    const getHeight = self.getHeight();

    // default
    self.$mainContainer
    .css('min-height', getHeight.siteWithFooter);

    // set custom
    self.$environment
    .each(function(index) {
      const $object             = $(this);
      const groupParams         = $object.attr('env-height').split(':');
      const envHeight           = groupParams[0];
      const envHeightLimit      = groupParams[1];
      const envHeightBreakpoint = (isNaN(groupParams[2])) ? 0 : groupParams[2];
      const windowWidth         = $(window).outerWidth();

      if(windowWidth >= envHeightBreakpoint) {

        switch(envHeightLimit) {
          case 'max':
            $object.css('height', getHeight[envHeight]);
          break;
          case 'min':
            $object.css('min-height', getHeight[envHeight]);
          break;
          default:
            return false;
          break;
        }

      } else {

        switch(envHeightLimit) {
          case 'max':
            $object.css('height', 'auto');
          break;
          case 'min':
            $object.css('min-height', 'auto');
          break;
          default:
            return false;
          break;
        }
      }
    });
  }

  set() {
    const self = this;
    self.constructor();
    let resizeTimer;

    // Execute
    self._setEnvironment();

    $(window)
    .on('load', function() {
      self._setEnvironment();
    });

    $(window)
    .on('resize', function(event) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(() => {
        self._setEnvironment();
      }, 250);
    });
  }
};



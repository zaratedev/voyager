/**
 * Preload images
 *
 * This receives an array of images and preloads
 */

/**
 * @param array | required
 *
 * Example:
 *
 * fn.load(['../logo.png', '../logo-2.png']);
 */

export class FNPreloadImages {

  _preload(images, index) {
    const self = this;

    // --

    index = index || 0;

    if(images && images.length > index) {
      const preloadedImg = new Image();

      preloadedImg.onload = function() {
        self._preload(images, index + 1);
      }

      preloadedImg.src = images[index];
    }
  }

  load(imagesToLoad) {
    const self = this;

    // --

    self._preload(imagesToLoad, 0);
  }
}

/**
 * Shuffle array
 *
 * This receives an array and return it shuffled
 */

/**
 * @param array | required
 *
 * Example:
 * console.log(fn.do(['a','b','c']));
 */

export class FNShuffleArray {

  _shuffle(array) {
    let i;
    let j;
    let temp;

    for(i = array.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1))
      temp = array[i]
      array[i] = array[j]
      array[j] = temp
    }

    return array;
  }

  do(arraytoShuffle) {
    const self = this;

    //--

    let shuffled = null;

    if(arraytoShuffle) {
      shuffled = self._shuffle(arraytoShuffle);
    }

    return shuffled;
  }
};

/**
 * Regular
 *
 * It makes all the elements with the same class
 * have the same height.
 */

/**
 * @attribute regular
 *   @param string  | childrenClass | required
 *   @param numeric | breakpoint    | optional
 *
 * Example:
 *
 * <div regular="childrenClass:breakpoint">
 *   <div class="childrenClass"></div>
 *   ...
 * </div>
 */

export class FNRegular {

  constructor() {
    this.$regular = $('[regular]');
  }

  _setRegular() {
    const self = this;
    self.constructor();

    // --

    self.$regular
    .each(function(index) {
      const $el = $(this);
      const groupParams        = $el.attr('regular').split(':');
      const childrenClass      = '.' + groupParams[0];
      const childrenBreakpoint = (isNaN(groupParams[1])) ? 0 : groupParams[1];
      const $children          = $el.find(childrenClass);
      const windowWidth        = $(window).outerWidth();

      let a = 0;
      let b = 0;

      if(windowWidth >= childrenBreakpoint) {
        $children.each(function(j) {
          $children.css('height', 'auto');

          a = $(this).outerHeight();

          if(a >= b) {
            b = a;
          }

          $children.css('height', b);
        });
      } else {
        $children.css('height', 'auto');
      }
    });
  }

  make() {
    const self = this;
    self.constructor();
    let resizeTimer;

    //--

    self._setRegular();

    $(window).on('load', function() {
      self._setRegular();
    });

    $(window)
    .on('resize', function(event) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(() => {
        self._setRegular();
      }, 250);
    });
  }

};

/**
 * Form validation
 *
 * Validations are automatic except for _onlyDigits
 */

/**
 * @attribute only-digits
 *
 * Example:
 *
 * <input type="text" only-digits value="...">
 */

const secureKeys = [
  'Backspace',
  'Escape',
  'Enter',
  'ArrowUp',
  'ArrowLeft',
  'ArrowRight',
  'ArrowDown',
  ' ',
  'CapsLock',
  'Shift',
  'Meta',
  'Alt',
  'Control',
  'Tab',
];

export class FNFormValidation {

  constructor() {
    this.$submitBtn   = $('button[type="submit"]');
    this.$fields      = $('input, select, textarea');
    this.$files       = $('[type="file"]');
    this.$onlyDigits  = $('[only-digits]');
    this.$onlyNumeric  = $('[only-numeric]');
    this.$select      = $('select:not(.selectized)');
  }

  _files($fileField) {
    if(!$fileField.is('[multiple]')) {
      const file = $fileField[0].files[0];

      //--

      $fileField
      .nextAll('label')
      .html(file.name);
    } else {
      const filesLength = $fileField[0].files.length;
      const desc  = filesLength > 1 ? 'imágenes seleccionadas' : 'imágen seleccionada' ;

      //--

      $fileField
      .nextAll('label')
      .html(filesLength + ' ' + desc);
    }
  }

  _isEmpty($field) {
    if($field.val()) {
      $field
      .addClass('filled')
      .parents('.input-field')
      .addClass('filled');
    } else{
      $field
      .removeClass('filled')
      .parents('.input-field')
      .removeClass('filled');
    }
  }

  _onlyDigits(press, $this) {
    const isValid = /^([0-9])$/.test(press.key);

    // --

    if(secureKeys.indexOf(press.key) < 0) {
      if(!isValid) {
        press.preventDefault();
      }
    }
  }

  _onlyNumeric(press, $this) {
    const isValid = /^([0-9\.\,])$/.test(press.key);

    // --

    if(secureKeys.indexOf(press.key) < 0) {
      if(!isValid) {
        press.preventDefault();
      }
    }
  }

  _preventMultipleSubmit($this) {
    $this
    .attr('disabled', true)
    .parents('form')
    .submit();
  }

  _selects($selectField) {
    const selectvalue = $selectField.find('option:selected').text();

    //--

    if($.trim(selectvalue)){
      $selectField
      .nextAll('label')
      .html(selectvalue);
    }
  }

  validate() {
    const self = this;

    self.constructor();

    //--

    self.$files
    .on('change', function(event) {
      self._files($(this));
    });

    // --

    self.$fields
    .each(function(index) {
      self._isEmpty($(this));
    });

    self.$fields
    .on('change', function(event) {
      self._isEmpty($(this));
    });

    // --

    self.$onlyDigits
    .keydown(function(press) {
      self._onlyDigits(press, $(this));
    });

    self.$onlyNumeric
    .keydown(function(press) {
      self._onlyNumeric(press, $(this));
    });

    // --

    self.$submitBtn
    .on('click', function(event) {
      self._preventMultipleSubmit($(this));
    });

    // --

    self.$select
    .each(function(index) {
      self._selects($(this));
    });

    self.$select
    .on('change', function(event) {
      self._selects($(this));
    });
  }
};


/**
 * Disable scrolling
 *
 * If you need disable or enable window scroll
 */

/**
 * Example:
 *
 * fn.enable();
 * fn.disable();
 */

export class FNScrolling {

  constructor() {
    this.$html           = $('html');
    this.$window         = $(window);
    this.classScrolling  = 'scrolling-disabled';
    this.windowScrollTop = this.$window.scrollTop();
    this.temp = 0;
  }

  disable() {
    const self = this;

    // --

    self.windowScrollTop = this.$window.scrollTop();

    // --

    if(!this.$html.hasClass(this.classScrolling)) {
      this.$html
        .addClass(this.classScrolling)
        .css({
          'position': 'fixed',
          'top': -this.windowScrollTop,
        });

      self.temp = self.windowScrollTop;
    }
  }

  enable() {
    const self = this;

    // --

    self.temp = (self.temp) ? self.temp : 0;

    // --

    if(this.$html.hasClass(this.classScrolling)) {
      this.$html
      .removeClass(this.classScrolling)
      .css({
        'position': 'relative',
        'top': 'auto',
      })
      .scrollTop(self.temp);
    }
  }
};

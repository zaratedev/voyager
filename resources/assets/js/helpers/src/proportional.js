/**
 * Proportional
 *
 * It makes the element height in proportion to the width.
 */

/**
 * @attribute proportional
 *   @param string  | rectangle/square | required
 *   @param string  | max              | optional
 *
 * Example:
 *
 * <div proportional="rectangle:max"></div>
 */

export class FNProportional {

  constructor() {
    this.$proportional = $('[proportional]');
  }

  _setProportion() {
    const self = this;
    self.constructor();

    // --

    $.each(self.$proportional, function(index) {
      const $el = $(this);
      const proportion = $el.attr("proportional").split(':');
      let proportionalWidth;

      switch(proportion[0]) {

        case 'rectangle':
          proportionalWidth = Math.round($el.outerWidth() / 2);
        break;

        case 'square':
          proportionalWidth  = $el.outerWidth();
        break;

        default:
          proportionalWidth = 0;
        break;
      }

      const property = (proportion[1]) ? 'height' : 'min-height';

      $el
      .css(property, proportionalWidth);
    });
  }

  make() {
    const self = this;
    self.constructor();
    let resizeTimer;

    // --

    self._setProportion();

    $(window)
    .on('load', function() {
      self._setProportion();
    });

    $(window)
    .on('resize', function(event) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(() => {
        self._setProportion();
      }, 250);
    });
  }

};

// --
//  Controller Loading
// --

const controllers = {
  home: require('./views/home'),
};

// --
//  Dispatch controller from data-controller tag.
//  Do not change anything.
// --

export class Controller {

  getDomController() {
    return $('[data-controller]:first').attr('data-controller');
  }

  controllerBinded(controller) {
    return typeof controller === typeof 'undefined' || controller === false;
  }

  dispatch() {
    const controller = this.getDomController();

    // For some browsers, 'attr' is undefined,
    // for others 'attr' is false
    // Check for both.
    if (this.controllerBinded(controller) && controller != "" && typeof controller != 'undefined') {
      controllers[controller].init();
    }
  }

};

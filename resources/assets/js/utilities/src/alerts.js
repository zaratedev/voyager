const defaultSettings = {
  animation        : 'zoom',
  backgroundDismiss: true,
  closeAnimation   : 'zoom',
  draggable        : false,
  smoothContent    : false,
  theme            : 'modern',
  typeAnimated     : false,
  useBootstrap     : false,
}

const defaultBtns = {
  buttons: {
    close: {
      text: 'Cerrar'
    }
  }
}

export class FNAlerts {

  constuctor() {
    this.$btnDialog = $('a');
  }

  errors() {
    let content = app.errors;

    if (content.constructor === Array) {
      content = '<ul><li>' + app.errors.join('</li><li>') + '</li></ul>';
    }

    let errorOptions = {
      defaultBtns,
      content: content,
      icon   : 'far fa-times-circle x:fs-50',
      title  : 'Hay un problema',
      type   : 'red',
    };

    errorOptions = $.extend(defaultSettings, errorOptions, defaultBtns);

    if (typeof app.errors != 'undefined' && app.errors.length > 0) {
      $.confirm(errorOptions);
    }
  }

  success() {
    const content = '<p>' + app.success + '</p>';

    let successOptions = {
      defaultBtns,
      content: content,
      icon   : 'far fa-check-circle x:fs-50',
      title  : 'Completado',
      type   : 'green',
    };

    successOptions = $.extend(defaultSettings, successOptions, defaultBtns);

    if (typeof app.success != 'undefined' && app.success.length > 0) {
      $.confirm(successOptions);
    }
  }
}

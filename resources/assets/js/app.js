import './bootstrap';

// Env
import { Controller } from './controllers';
const frontController = new Controller();

// We'll load jQuery and libraries.
try {
  window.$ = window.jQuery = require('jquery');

  require('jquery-confirm');
  // require('../../../node_modules/...');
} catch (e) {}

// Helpers
import {
  FNEnvironment
} from './helpers';

// Utilities
import {
  FNAlerts
} from './utilities';

// Define
const fnEnvironment = new FNEnvironment;
const fnAlerts = new FNAlerts;

// Initializers
function docReady() {
  // Framework initializers
  frontController.dispatch();

  // App & Utilities & Helpers
  fnAlerts.errors();
  fnAlerts.success();
}

function winLoad() {
  // Framework initializers
  // ...

  // App & Utilities & Helpers
  // ...
}

function docReadyWinload() {
  // Framework initializers
  fnEnvironment.set();

  // App & Utilities & Helpers
  // ...
}

function resize() {
  // Framework initializers
  // ...

  // App & Utilities & Helpers
  // ...
}

// [IMPORTANT] Do not change or add anything!
$(document).ready(function() {
  docReady();
  docReadyWinload();
  resize();
  $(window).resize(function() {
    resize();
  });
});
$(window).on('load', function() {
  winLoad();
  docReadyWinload();
  resize();
  $(window).resize(function() {
    resize();
  });
});

// Watermark

console.log(
  "%c ",
  "background: url(https://dinkbit.s3.amazonaws.com/img/firmas/dinkbit.png) no-repeat center/150px; color: white; padding: 40px 75px"
)

@php
  $page = TCG\Voyager\Models\Page::first();
@endphp

@can('browse', $page)
  You can browse pages
@else
  You do not have access to edit pages
@endcan
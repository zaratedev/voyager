@if (!empty(config('captcha.sitekey')))
  {!! NoCaptcha::display() !!}
@endif

{{-- Docs: https://github.com/anhskohbo/no-captcha --}}
{{-- Custom attributes: https://developers.google.com/recaptcha/docs/display#render_param --}}

{{-- Validate --}}
{{-- 'g-recaptcha-response' => 'required|captcha' --}}

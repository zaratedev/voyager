@if (!empty(config('captcha.sitekey')))
  {!! NoCaptcha::renderJs(config('app.locale')) !!}
@endif

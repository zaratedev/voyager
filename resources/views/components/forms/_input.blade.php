{{--
/**
 * Input partial
 *
 * ...
 *
 * @param array           attributes
 * @param array           labelAttributes
 * @param boolean         autocomplete
 * @param boolean         readonly
 * @param boolean         required
 * @param boolean         disable
 * @param string          containerClass
 * @param string          inputClass
 * @param string          idName          required
 * @param boolean         withoutName
 * @param string          type            required
 * @param string          hasIco          example: fa fa-home
 * @param string          hasCurrency
 * @param string|number   value
 * @param string|number   index
 * _________
 *
 * hidden type input params
 *
 * @param string value  required
 * _________
 *
 * radio|checkbox type input params
 *
 * @param boolean       checked
 * _________
 *
 * checkbox type input params
 *
 * @param string idName example: ...[] *
 * _________
 *
 * Extra params
 *
 * @param string|html   label           required
 * _________
 *
 * More attributes
 *
 * https://www.w3schools.com/tags/tag_input.asp
 * _________
 *
 * @dinkbit-todo revisar sobre proyecto real y
 * realizar ajustes de ser necesario
 *
 */
--}}
@php
  /* Validate if type is radio|checkbox|hidden */
  $typeRCH = $type == 'radio' ||
             $type == 'checkbox' ||
             $type == 'hidden';
  $typeRC = $type == 'radio' ||
             $type == 'checkbox';
  /* Validate old() */
  if(!$typeRCH) {
    $value = (isset($value)) ? $value : old($idName, '');
  }
  else {
    $checkedOld = old($idName);
  }
  /* index required */
  if($typeRC) {
    $index = $index;
  }
@endphp
@if($type != 'hidden')
  <div
    class="
      input-field
      {{ $type }}
      {{ (!empty($containerClass)) ? $containerClass : '' }}
      {{ ($errors->has($idName)) ? 'error' : '' }}
      {{ (!empty($hasIco)) ? 'has-ico' : '' }}
      {{ (!empty($hasCurrency)) ? 'has-currency' : '' }}
      {{ (!empty($button)) ? 'button' : '' }}
      @if(!$typeRCH)
        {{ ($value) ? 'filled' : '' }}
      @endif
    "
  >
@endif
  <input
    id    = "{{ $idName }}{{ (!empty($index)) ? '-'.$index : '' }}"
    name  = "{{ (!empty($withoutName)) ? '' : $idName }}"
    type  = "{{ $type }}"
    class = "
      @if(!$typeRCH)
        {{ ($value) ? 'filled' : '' }}
      @endif
      {{ ($errors->has($idName)) ? 'error' : '' }}
      {{ (!empty($inputClass)) ? $inputClass : '' }}
      {{ (!empty($button['ico'])) ? 'btn-ico' : '' }}
      {{ (!empty($button['label'])) ? 'btn-label' : '' }}
    "
    @if(!empty($attributes))
      @foreach($attributes as $attribute => $val)
        {{ $attribute }}='{{ $val }}'
      @endforeach
    @endif
    {!! (!empty($autocomplete)) ? 'autocomplete="on"' : 'autocomplete="new-'.$idName.'"' !!}
    {{ (!empty($disabled)) ? 'disabled' : '' }}
    {{ (!empty($readonly)) ? 'readonly' : '' }}
    {{ (!empty($required)) ? 'required' : '' }}
    @if(!$typeRCH)
      {!! (isset($value) && $value != '') ? 'value="'.$value.'"' : '' !!}
    @else
      value = "{{ $value }}"
      {{ (!empty($checked) || $checkedOld == $value) ? 'checked' : '' }}
    @endif
  >
  @if($type != 'hidden')
    <label
      for="{{ $idName }}{{ (!empty($index)) ? '-'.$index : '' }}"
      @if(!empty($labelAttributes))
        @foreach($labelAttributes as $labelAttribute => $val)
          {{ $labelAttribute }}='{{ $val }}'
        @endforeach
      @endif
    >{!! $label !!}</label>
    {!! (!empty($hasIco)) ? "<i class='".$hasIco."'></i>" : '' !!}
    @if (!empty($hasCurrency))
      <span class="symbol">$</span>
      <span class="currency">{{ $hasCurrency }}</span>
    @endif
  @endif
@if($type != 'hidden')
  </div>
@endif

{{--

/**
 * Select partial
 *
 * Generic params
 *
 * @param array       attributes
 * @param array       labelAttributes
 * @param array       options         required|['value' => 'label']
 * @param array       optionsGroup    [[name => '', options[...], [...]]
 * @param boolean     disabled
 * @param boolean     required
 * @param boolean     na
 * @param string      containerClass
 * @param string      idName          required
 * @param string      itemSelected
 * @param string      hasIco          example: fa fa-home
 * @param string|html label           required
 * _________
 *
 * More attributes
 *
 * https://www.w3schools.com/tags/tag_select.asp
 * _________
 *
 * @dinkbit-todo revisar sobre proyecto real y realizar ajustes de ser necesario
 *
 */

--}}

@php
  /* Validate old() */
  $itemSelected = (isset($itemSelected)) ? $itemSelected : old($idName, '');
@endphp

<div
  class="
    input-field
    select
    {{ (!empty($containerClass)) ? $containerClass : '' }}
    {{ ($errors->has($idName)) ? 'error' : '' }}
    {{ (!empty($hasIco)) ? 'has-ico' : '' }}
    {{ ($itemSelected) ? 'filled' : '' }}
  "
>
  <select
    id    = "{{ $idName }}{{ (!empty($index)) ? '-'.$index : '' }}"
    name  = "{{ $idName }}"
    class = "
    {{ ($itemSelected) ? 'filled' : '' }}
    {{ ($errors->has($idName)) ? 'error' : '' }}
    "

    @if(!empty($attributes))
      @foreach($attributes as $attribute => $val)
        {{ $attribute }}='{{ $val }}'
      @endforeach
    @endif

    {{ (!empty($disabled)) ? 'disabled' : '' }}
    {{ (!empty($required)) ? 'required' : '' }}
  >

    <option
      disabled
      selected
      style="display:none;"
    >
    </option>

    @if(!empty($na))
      <option value="N/A">N/A</option>
    @endif

    @foreach($options as $optionValue => $optionLabel)
      <option
        value="{{ $optionValue }}"
        {{ ($optionValue === '0' || !empty($optionValue)) ? '' : 'disabled' }}
        {{ (string)$itemSelected === (string)$optionValue ? 'selected' : '' }}
      >
        {{ $optionLabel }}
      </option>
    @endforeach

    @if(!empty($optionsGroup))
      @foreach($optionsGroup as $group)
        <optgroup label="{{ $group['name'] }}">
          @foreach($group['options'] as $optionValue => $optionLabel)
            <option
              value="{{ $optionValue }}"
              {{ ($optionValue === '0' || !empty($optionValue)) ? '' : 'disabled' }}
              {{ (string)$itemSelected === (string)$optionValue ? 'selected' : '' }}
            >
              {{ $optionLabel }}
            </option>
          @endforeach
        </optgroup>
      @endforeach
    @endif

  </select>

  <label
    for="{{ $idName }}{{ (!empty($index)) ? '-'.$index : '' }}"

    @if(!empty($labelAttributes))
      @foreach($labelAttributes as $labelAttribute => $val)
        {{ $labelAttribute }}='{{ $val }}'
      @endforeach
    @endif

  >{!! $label !!}</label>
  {!! (!empty($hasIco)) ? "<i class='".$hasIco."'></i>" : '' !!}

</div>


{{-- <div class="test">
  @foreach($options as $optionValue => $optionLabel)
    @php
      $itemSelected = (string)$itemSelected;
      $optionValue = (string)$optionValue;
    @endphp
    @if ($itemSelected === $optionValue)
      <p>{{ gettype($itemSelected) }} | {{ gettype($optionValue) }}=> {{ $itemSelected }} === {{ $optionValue }} True</p>
    @else
      <p>{{ gettype($itemSelected) }} | {{ gettype($optionValue) }}=> {{ $itemSelected }} === {{ $optionValue }} False</p>
    @endif
  @endforeach
</div> --}}

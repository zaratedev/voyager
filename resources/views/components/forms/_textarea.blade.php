{{--

/**
 * Textarea partial
 *
 * Generic params
 *
 * @param array       attributes
 * @param array       labelAttributes
 * @param boolean     disable
 * @param boolean     required
 * @param boolean     readonly
 * @param string      inputClass
 * @param string      containerClass
 * @param string      idName          required
 * @param string      hasIco          example: fa fa-home
 * @param string      value
 * @param string|html label           required
 * _________
 *
 * More attributes
 *
 * https://www.w3schools.com/tags/tag_textarea.asp
 * _________
 *
 * @dinkbit-todo revisar sobre proyecto real y realizar ajustes de ser necesario
 *
 */

--}}

@php
  /* Validate old() */
  $value = (!empty($value)) ? $value : old($idName, '');
@endphp

<div
  class="
    input-field
    textarea
    {{ (!empty($containerClass)) ? $containerClass : '' }}
    {{ ($errors->has($idName)) ? 'error' : '' }}
    {{ (!empty($hasIco)) ? 'has-ico' : '' }}
    {{ ($value) ? 'filled' : '' }}
  "
>
  <textarea
    id    = "{{ $idName }}"
    name  = "{{ $idName }}"
    class = "
        {{ ($value) ? 'filled' : '' }}
        {{ ($errors->has($idName)) ? 'error' : '' }}
        {{ (!empty($inputClass)) ? $inputClass : '' }}
    "

    {{ (!empty($disabled)) ? 'disabled' : '' }}
    {{ (!empty($readonly)) ? 'readonly' : '' }}
    {{ (!empty($required)) ? 'required' : '' }}

    @if(!empty($attributes))
      @foreach($attributes as $attribute => $val)
        {{ $attribute }}='{{ $val }}'
      @endforeach
    @endif

  >{{ $value }}</textarea>

  <label
    for="{{ $idName }}"

    @if(!empty($labelAttributes))
      @foreach($labelAttributes as $labelAttribute => $val)
        {{ $labelAttribute }}='{{ $val }}'
      @endforeach
    @endif

  >{!! $label !!}</label>
  {!! (!empty($hasIco)) ? "<i class='".$hasIco."'></i>" : '' !!}

</div>

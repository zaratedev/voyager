<head>

  <title>@yield('title', '')</title>

  {{-- META TAGS --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">

  <meta name="author" content=""/>
  <meta name="keywords" content="@yield('keywords', '')"/>
  <meta name="description" content="@yield('description', '')"/>

  <meta property="og:title" content="@yield('title', '')"/>
  <meta property="og:description" content="@yield('description', '')"/>
  <meta property="og:type" content="website"/>
  <meta property="og:locale" content="es_LA"/>
  <meta property="og:url" content="{{ Request::url() }}"/>
  <meta property="og:site_name" content="{{ config('app.name') }}"/>
  <meta property="og:image" content="@yield('image', asset('assets/img/ico/logo-share.jpg'))"/>
  <meta property="og:image:width" content="1200"/>
  <meta property="og:image:height" content="630"/>

  @yield('meta')

  {{-- FAV and TOUCH ICONS --}}
  {{-- http://www.favicon-generator.org/ --}}
  <link rel="shortcut icon" href="{{ asset('assets/img/ico/favicon.ico') }}" type="image/x-icon"/>

  {{-- APPLE ICONS --}}
  <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/ico/apple-icon-57x57.png') }}"/>
  <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/ico/apple-icon-60x60.png') }}"/>
  <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/ico/apple-icon-72x72.png') }}"/>
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/ico/apple-icon-76x76.png') }}"/>
  <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/ico/apple-icon-114x114.png') }}"/>
  <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/ico/apple-icon-120x120.png') }}"/>
  <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/ico/apple-icon-144x144.png') }}"/>
  <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/ico/apple-icon-152x152.png') }}"/>
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/ico/apple-icon-180x180.png') }}"/>

  {{-- ANDROID ICONS --}}
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/ico/favicon-16x16.png') }}"/>
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/ico/favicon-32x32.png') }}"/>
  <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/ico/favicon-96x96.png') }}"/>
  <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/ico/android-icon-192x192.png') }}"/>
  <link rel="manifest" href="{{ asset('assets/img/ico/manifest.json') }}">

  {{-- MICROSOFT ICONS --}}
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{ asset('assets/img/ico/ms-icon-144x144.png') }}">
  <meta name="theme-color" content="#ffffff">

  {{-- CSS --}}
  {{-- <link href="https://fonts.googleapis.com/css?family=???" rel="stylesheet"> --}}
  <link rel="stylesheet" href="{{ mix('css/base.css', 'assets') }}">
  <link rel="stylesheet" href="{{ mix('css/app.css', 'assets') }}">

  @yield('css')

  {{-- SCRIPTS --}}
  <script src="{{ mix('js/manifest.js', 'assets') }}"></script>
  <script src="{{ mix('js/global.js', 'assets') }}"></script>
  <script src="{{ mix('js/vendor.js', 'assets') }}"></script>
  <script src="{{ mix('js/app.js', 'assets') }}"></script>

  {{-- PIXELS --}}
  @if(config('app.env') == 'production')
    @include('components.google._analytics')
    @include('components.google._calls')
    @include('components.google._tag-manager-head')
    @include('components.facebook._pixel')
  @endif

  @include('components.google._recaptcha-js')

  {{-- ADITIONAL SCRIPTS --}}
  @yield('js_head')

</head>

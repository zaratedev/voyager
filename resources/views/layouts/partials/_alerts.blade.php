@if(Request::get('success') || Session::has('success'))

  @if(Request::get('success'))
    <script>
      app.success = '{{ (Request::get('success')) ? Request::get('success') : 'Operación realizada exitosamente.' }}'
    </script>
  @elseif(Session::has('success'))
    <script>
      app.success = '{{ (Session::get('success')) ? Session::get('success') : 'Operación realizada exitosamente.' }}'
    </script>
  @endif

@endif

@if($errors->any())
  <script>

    @foreach($errors->all() as $error)
      app.errors.push('{{ $error }}')
    @endforeach

  </script>
@endif

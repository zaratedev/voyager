<!DOCTYPE html>

<!--
  __              __      __          __
 /\ \  __        /\ \    /\ \      __/\ \__
 \_\ \/\_\    ___\ \ \/'\\ \ \____/\_\ \ ,_\
 /'_` \/\ \ /' _ `\ \ , < \ \ '__`\/\ \ \ \/
/\ \L\ \ \ \/\ \/\ \ \ \\`\\ \ \L\ \ \ \ \ \_
\ \___,_\ \_\ \_\ \_\ \_\ \_\ \_,__/\ \_\ \__\
 \/__,_ /\/_/\/_/\/_/\/_/\/_/\/___/  \/_/\/__/
{{ config('app.developers') }}
-->

<html lang="{{ app()->getLocale() }}">

@include('layouts.partials._head')

  <body id="@yield('body-id', '')">

    @if(config('app.env') == 'production')
      @include('components.google._tag-manager-body')
    @endif

    <!--[if lte IE 9]>
      <p class="chromeframe">Estás usando un navegador <strong>antiguo</strong>. Por favor <a href="http://browsehappy.com/">actualiza tu navegador</a> para visualizar el sitio correctamente.</p>
    <![endif]-->

    <div class="main-container @yield('main-class', '')" data-controller="@yield('data-controller', '')">
      @include('layouts.partials._nav')
      @include('layouts.partials._alerts')
      @yield('content')
    </div>

    <footer>
      @include('layouts.partials._footer')
    </footer>

    @yield('js_footer')

    <noscript class="noscript">
      <div class="content">
        <p>Para utilizar las funcionalidades completas de {{ config('app.name') }} es necesario tener JavaScript habilitado.</p>
      </div>
    </noscript>

  </body>
</html>

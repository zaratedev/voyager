<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <title>Ha ocurrido un error</title>
</head>
<body>

  <div id="content">
    <p>Lo sentimos, ha ocurrido un error</p>
    <br/>
    <a href="/">￩ regresar a página de inicio</a>
  </div>

  <style>

    * {
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;

      outline: none;
      outline: 0;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }

    html,
    body {
      background-color: #f4f4f4;
      color: #888;
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Source Sans Pro", Oxygen, sans-serif;;
      font-size: 16px;
      font-weight: lighter;
      height: 100%;
      margin: 0 auto;
      padding: 0px;
      position: relative;
      transition: all .3s ease;
      width: 100%;
    }

    #content {
      left: 50%;
      padding: 20px;
      position: absolute;
      text-align: center;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 100%;
    }

    h1 {
      color: #057df7;
      font-size: 80px;
      font-weight: lighter;
      margin: 0px;
      padding: 5px;
      transition: all .3s ease;
    }

    p {
      font-size: 40px;
      font-weight: lighter;
      margin: 0px;
      padding: 5px;
      transition: all .3s ease;
    }

    a {
      color: #ccc;
      font-size: 24px;
      text-decoration: none;
      transition: all .3s ease;
    }

    a:hover {
      color: #E67928;
    }

    @media (max-width: 767px) {

      h1 {
        font-size: 50px;
      }

      p {
        font-size: 30px;
      }
    }

    @media (max-width: 599px) {

      h1 {
        font-size: 40px;
      }

      p {
        font-size: 24px;
      }
    }

  </style>

</body>
</html>


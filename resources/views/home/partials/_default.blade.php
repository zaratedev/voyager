<div id="dinkbit">
  <img src="{{ Voyager::image( setting('site.logo') ) }}" alt="">
  <h1>{{ setting('site.title') }}</h1>
  <p>¡Ahora puedes hacer cosas increibles!</p>
  <div class="kc-board">
    <span class="kc up">↑</span>
    <span class="kc up">↑</span>
    <span class="kc down">↓</span>
    <span class="kc down">↓</span>
    <span class="kc left">←</span>
    <span class="kc right">→</span>
    <span class="kc left">←</span>
    <span class="kc right">→</span>
    <span class="kc b">B</span>
    <span class="kc a">A</span>
  </div>
</div>

<style>

  * {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  html,
  body {
    background-color: #f4f4f4;
    color: #888;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Source Sans Pro", Oxygen, sans-serif;;
    font-size: 16px;
    font-weight: lighter;
    height: 100%;
    margin: 0 auto;
    padding: 0px;
    position: relative;
    transition: all .3s ease;
    width: 100%;
  }

  html.blue,
  body.blue {
    background-color: #057df7;
  }

  body.blue h1 {
    color: #70b6ff;
  }

  body.blue p {
    color: #70b6ff;
  }

  body.blue .kc-board {
    color: #70b6ff;
  }

  body.blue .press {
    color: #fff;
  }

  #dinkbit {
    left: 50%;
    padding: 20px;
    position: absolute;
    text-align: center;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
  }

  h1 {
    color: #057df7;
    font-size: 130px;
    font-weight: lighter;
    margin: 0px;
    padding: 5px;
    transition: all .3s ease;
  }

  p {
    font-size: 40px;
    font-weight: lighter;
    margin: 0px;
    padding: 5px;
    transition: all .3s ease;
  }

  .kc-board {
    font-size: 40px;
    font-weight: bold;
    margin-top: 40px;
    transition: all .3s ease;
  }

  .press {
    color: #057df7;
  }

  .press-fail {
    color: #F44336;
  }

  @media (max-width: 767px) {

    h1 {
      font-size: 100px;
    }

    p {
      font-size: 30px;
    }
  }

  @media (max-width: 599px) {

    h1 {
      font-size: 80px;
    }

    p {
      font-size: 24px;
    }
  }

</style>

<script>

  fn_konamiCode = {

    _construc: function() {
      _kcThis = this;

      // --

      kcValid = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        65: 'a',
        66: 'b'
      };

      kc = ['up','up','down','down','left','right','left','right','b','a'];

      kcPos = 0;
    },

    _compare: function(press) {
      keyPressed = kcValid[press.which];
      keyRequired = kc[kcPos];

      if(keyPressed == keyRequired) {

        // ...
        _kcThis._kcBoard(kcPos, true);
        //...

        kcPos++;

        if(kcPos == kc.length) {
          _kcThis._launch();
          kcPos = 0;

          // ...
          _kcThis._kcBoard(kcPos, false);
          //...
        }
      }
      else {
        kcPos = 0;

        // ...
        _kcThis._kcBoard(kcPos, false);
        //...
      }
    },

    _kcBoard: function(kcPos, succes) {
      $kcBoard = $('.kc-board span');
      $kcBoardChild = $('.kc-board span:eq('+ kcPos +')');

      if(succes) {
        $kcBoardChild
        .addClass('press');
      }
      else {
        $kcBoard
        .removeClass('press');
      }
    },

    _launch: function() {
      $('html, body')
      .toggleClass('blue');
    },

    init: function() {
      this._construc();

      // --

      console.log('dinkbit: ↑↑ ↓↓ ← → ← → BA')

      $(document)
      .on('keydown', function(press) {
        _kcThis._compare(press);
      });

    }
  }

  document.addEventListener(
    'DOMContentLoaded',
    function() {
      fn_konamiCode.init();
    },
  false);

</script>


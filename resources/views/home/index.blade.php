@extends('layouts.master')
@section('title', config('app.name').' :: ...')
@section('body-id', '')
@section('main-class', 'home')

{{-- @section('data-controller', '') --}}
{{-- @section('description', '') --}}
{{-- @section('keywords', '')" --}}
{{-- @section('image', src) --}}
{{-- @section('meta') ... @stop --}}
{{-- @section('css', src) --}}
{{-- @section('js_head') ... @stop --}}

@section('content')

  {!! menu('main', 'home.partials._menu') !!}
  @include('home.partials._default')

@stop

{{-- @section('js_footer') ... @stop --}}

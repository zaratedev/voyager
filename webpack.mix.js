
// Require
let mix = require('laravel-mix');

// Mix
mix

// Set directories
.setResourceRoot('../')
.setPublicPath('public/assets/')

// Options
.options({
  fileLoaderDirs: {
    images: 'img',
  }
})

// Styles
.less(
  'resources/assets/less/base.less',
  'css/base.css'
)

.less(
  'resources/assets/less/app.less',
  'css/app.css'
)

// Default scripts
.js(
  'resources/assets/js/global.js',
  'js/global.js'
)

.js(
  'resources/assets/js/app.js',
  'js/app.js'
)

// Extract libraries
.extract([
  'jquery',
  'axios',
  'lodash'
])

// Autoload
.autoload({
  jquery: [
    '$',
    'window.jQuery',
    'jQuery',
    'jquery'
  ]
});

if(mix.inProduction()) {
  // Mix
  mix

  // Source maps
  .sourceMaps()

  // Version
  .version();
}

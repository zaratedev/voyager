<?php

namespace App\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * This is the current route name composer class.
 *
 * @author Rafael Masri <rafael.masri@dinkbit.com>
 */
class CurrentUserComposer
{
    /**
     * Create a new current route name composer instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('currentUser', Auth::user());
    }
}

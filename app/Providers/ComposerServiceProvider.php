<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * This is the view composer service provider class.
 *
 * @author Rafael Masri <rafael.masri@dinkbit.com>
 */
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->view->composer('*', 'App\Composers\CurrentRouteNameComposer');
        $this->app->view->composer('*', 'App\Composers\CurrentUserComposer');
    }
}

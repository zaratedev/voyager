<?php

/*
| Web Routes
*/

Route::get('/', [
    'as'   => 'home.index',
    'uses' => 'HomeController@index',
]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('pages', function () {
    return view('pages');
});
